package id.teguh.training.pigeon;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.teguh.training.pigeon.service.GmailApiService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PigeonApplicationTests {

	@Autowired private GmailApiService gmailApiService;
	@Autowired private MustacheFactory mustacheFactory;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testSendEmail() {
		gmailApiService.sendEmail(
				"Teguh Eka Putra",
				"teguh.putra@myrepublic.net",
				"Email Percobaan " + LocalDateTime.now(),
				"Ini email percobaan dikirim dari aplikasi"
		);
	}

	@Test
	public void testSendEmailWithTemplate(){
		Mustache templateEmail = mustacheFactory.compile("templates/welcome.html");
		Map<String, String> data = new HashMap<>();
		data.put("nama", "Julius");
		data.put("pesan", "Anda telah mendapatkan mobil terbaru");

		StringWriter output = new StringWriter();
		templateEmail.execute(output, data);

		gmailApiService.sendEmail(
				"Teguh Eka Putra",
				"teguh.putra@myrepublic.net",
				"testing aja",
				output.toString());
	}

}
