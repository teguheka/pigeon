package id.teguh.training.pigeon.util;

import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.users.FullAccount;
import org.apache.commons.io.IOUtils;

import java.io.FileOutputStream;
import java.io.IOException;


public class SandboxDropbox {
    public static void main(String args[]) throws DbxException, IOException {
        // Create Dropbox client
        DbxRequestConfig config = new DbxRequestConfig("dropbox/java-tutorial", "en_US");
        DbxClientV2 client = new DbxClientV2(config, "YOUR ACCESS TOKEN");
        FullAccount account = client.users().getCurrentAccount();
        System.out.println(account.getName().getDisplayName());

        // Get files and folder metadata from Dropbox root directory
        ListFolderResult result = client.files().listFolder("");
        while (true) {
            for (Metadata metadata : result.getEntries()) {
                System.out.println(metadata.getPathLower());
                System.out.println(metadata.getName());
            }

            if (!result.getHasMore()) {
                break;
            }

            result = client.files().listFolderContinue(result.getCursor());
        }

        // Upload "test.txt" to Dropbox
        /*try (InputStream in = new FileInputStream("StoredCredential")) {
            FileMetadata metadata = client.files().uploadBuilder("/StoredCredential").uploadAndFinish(in);
        }*/

        DbxDownloader<FileMetadata> downloader = client.files().download("/client_secret.json");
        try {
            byte[] bytes = IOUtils.toByteArray(downloader.getInputStream());

            //tested
            FileOutputStream out = new FileOutputStream("client_secret.json");
            downloader.download(out);
            out.close();
        } catch (DbxException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
