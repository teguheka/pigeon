package id.teguh.training.pigeon.controller;

import id.teguh.training.pigeon.dto.SendEmailDto;
import id.teguh.training.pigeon.service.GmailApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/v1/mail")
@Api(description = "Set of endpoints for get list email service.")
public class GmailApiController {

    @Autowired private GmailApiService gmailApiService;


    @PostMapping("/send")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("${gmailapicontroller.sendEmail}")
    public Map sendEmail(@RequestBody @Valid SendEmailDto sendEmailDto) {

        try {
            gmailApiService.sendEmail(sendEmailDto.getFrom(), sendEmailDto.getTo(), sendEmailDto.getSubject(), sendEmailDto.getContent());

            Map<String, Object> result = new HashMap<>();
            result.put("success", Boolean.TRUE);
            result.put("message", "success send email to " + sendEmailDto.getTo());
            return result;
        } catch (Exception ex) {
            throw new RuntimeException("failed: " + ex.getMessage());
        }
    }
}
