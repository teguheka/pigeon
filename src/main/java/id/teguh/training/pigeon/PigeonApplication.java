package id.teguh.training.pigeon;

import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.MustacheFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

@SpringBootApplication
public class PigeonApplication {

	public static void main(String[] args) {
		SpringApplication.run(PigeonApplication.class, args);
	}

	private static final String CLIENT_SECRET_JSON_FILE = "client_secret.json";
	private static final String STORED_CREDENTIAL_FILE = "StoredCredential";

	@Value("${GMAIL_CREDENTIAL_FILE_SERVER}")
	private String credentialFileServer;

	@Value("${DROPBOX_ACCESS_TOKEN}")
	private String dropboxAccessToken;

	@Value("${gmail.folder}")
	private String dataStoreFolder;

	@Bean
	public MustacheFactory mustacheFactory(){
		return new DefaultMustacheFactory();
	}

	@Bean
	public JsonFactory jsonFactory(){
		return JacksonFactory.getDefaultInstance();
	}

	@Bean @Profile("!heroku")
	public GoogleClientSecrets localFileClientSecrets() throws Exception {
		return GoogleClientSecrets.load(jsonFactory(), new InputStreamReader(new FileInputStream(dataStoreFolder + File.separator + CLIENT_SECRET_JSON_FILE)));
	}

	@Bean @Profile("heroku")
	public GoogleClientSecrets downloadClientSecrets() throws Exception {
		//downloadCredentialsFile(CLIENT_SECRET_JSON_FILE);
		//downloadCredentialsFile(STORED_CREDENTIAL_FILE);
		downloadCredentialsFile();
		return GoogleClientSecrets.load(jsonFactory(), new InputStreamReader(new FileInputStream(dataStoreFolder + File.separator + CLIENT_SECRET_JSON_FILE)));
	}

	//download from ngrok server
	/*private void downloadCredentialsFile(String filename) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseEntity<byte[]> response = restTemplate.exchange(credentialFileServer + "/" + filename, HttpMethod.GET, entity, byte[].class, "1");

		if (response.getStatusCode() == HttpStatus.OK) {
			Files.createDirectories(Paths.get(dataStoreFolder));
			Files.write(Paths.get(dataStoreFolder + File.separator + filename), response.getBody());
		}
	}*/

	private void downloadCredentialsFile() throws DbxException, IOException {
		Files.createDirectories(Paths.get(dataStoreFolder));

		DbxRequestConfig config = new DbxRequestConfig("dropbox/java-tutorial", "en_US");
		DbxClientV2 client = new DbxClientV2(config, dropboxAccessToken);
		//FullAccount account = client.users().getCurrentAccount();

		ListFolderResult result = client.files().listFolder("");
		while (true) {
			for (Metadata metadata : result.getEntries()) {
				DbxDownloader<FileMetadata> downloader = client.files().download(metadata.getPathLower());
				byte[] bytes = IOUtils.toByteArray(downloader.getInputStream());
				Files.write(Paths.get(dataStoreFolder + File.separator + metadata.getName()), bytes);
			}

			if (!result.getHasMore()) {
				break;
			}

			result = client.files().listFolderContinue(result.getCursor());
		}

	}
}
