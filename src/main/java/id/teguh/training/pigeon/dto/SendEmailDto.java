package id.teguh.training.pigeon.dto;

import javax.validation.constraints.NotNull;

public class SendEmailDto {
    @NotNull
    private String from;

    @NotNull
    private String to;

    @NotNull
    private String subject;

    @NotNull
    private String content;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
