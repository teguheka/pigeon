Step by step:
1. login google email yang akan dijadikan pengirim email
2. buka halaman https://console.developers.google.com/
3. buat project baru
4. enable gmail API
5. Klik menu Credentials di panel kiri
6. Ada beberapa pilihan jenis credential, kita pilih yang OAuth
7. Mengkonfigurasikan dulu OAuth Consent Screen, yaitu halaman konfirmasi yang akan ditampilkan Google pada waktu meminta ijin dari user. (isi email address dan product name)
8. pilih jenis aplikasi yang akan dibuat (pilih other dan ketik nama aplikasi, bebas tidak ada standar untuk penamaan)
9. download file credential
10. generate project from start.spring.io (modul: web, lombok, mail, devtools)
11. tambah library gmail
        <dependency>
          <groupId>com.google.api-client</groupId>
          <artifactId>google-api-client</artifactId>
          <version>1.23.0</version>
        </dependency>
        <dependency>
          <groupId>com.google.oauth-client</groupId>
          <artifactId>google-oauth-client-jetty</artifactId>
          <version>1.23.0</version>
        </dependency>
        <dependency>
          <groupId>com.google.apis</groupId>
          <artifactId>google-api-services-gmail</artifactId>
          <version>v1-rev73-1.23.0</version>
        </dependency>
12. siapkan service class java nya
13. Aplikasi kita membutuhkan dua konfigurasi agar kita bisa menggunakan GMail API:
    - lokasi file client_secret.json yang sudah kita unduh di langkah sebelumnya
    - lokasi folder tempat penyimpanan file hasil otorisasi
14. sediakan folder ${HOME}/.gmail-api/credentials untuk lokasi folder
15. tambahkan configurasi di application.properties
        spring.application.name=notifikasi-gmail
        gmail.account.username=endy.muhardin@gmail.com
        gmail.folder=${user.home}/.gmail-api/credentials
        gmail.credential=${gmail.folder}/client_secret.json
16. sediakan folder (saya menggunakan mac untuk project ini), tuliskan di terminal
    mkdir -p ~/.gmail-api/credentials
    cp ~/Downloads/client*.json ~/.gmail-api/credentials/client_secret.json
    ls -lR ~/.gmail-api

    seharusnya output

    total 0
    drwxr-xr-x  3 teguh  staff  96 Nov 13 10:28 credentials
    /Users/myrepublicid/.gmail-api/credentials:
    total 8
    -rw-r--r--@ 1 teguh  staff  430 Nov 13 10:28 client_secret.json

17. lengkapi code gmail service
18. lalu jalankan mvn clean spring-boot:run di terminal
19. buka url yang terdapat di console dan allow
20. setelah di allow akan ada file baru di ${user.home}/.gmail-api/credentials yaitu StoredCredential
21. mengirim dengan template bisa menggunakan Send With Us (https://www.sendwithus.com/)
22. taruh file template di src/main/resources/templates (saya menggunakan welcome.html)
23. tambahkan mustache library untuk memasang variable di template

        <dependency>
          <groupId>com.github.spullara.mustache.java</groupId>
          <artifactId>compiler</artifactId>
          <version>0.9.5</version>
        </dependency>
24. gunakan {{nama variabel}} pada template


note deploy to heroku
cd ~/.gmail-api/credentials
python -m SimpleHTTPServer 8000
./ngrok http 8000
heroku config:set SPRING_PROFILES_ACTIVE=heroku
heroku config:set GMAIL_CREDENTIAL_FILE_SERVER=https://646f1763.ngrok.io
